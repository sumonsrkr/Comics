﻿

FetchComicDetails(_baseAPIUrl + "comics/" + GetQueryStringByName("id") + "?" + _apiKey);

function FetchComicDetails(requestUrl) {
    var XHR = new HttpRequest(requestUrl, null);
    XHR.executeRequest(function (result) {
        GetDetailLayoutForComic(result.data.results[0]).then(sectionElement => {
            AddNewElement(sectionElement, "comicContainer");
        });
    });
}