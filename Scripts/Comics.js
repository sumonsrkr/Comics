﻿
var currentPage = GetQueryStringByName("page");
if (currentPage == null || currentPage == "") {
    currentPage = 1;
}
var offset = (currentPage - 1) * _pageSize;

FetchListOfComics(GenerateApiURL(_dataTypes.comics) + "&offset=" + offset, "", "");

function FilterComics(namesStartWith, titleStartWith) {
    var characterIds = "";
    GetCharacterIdsFilterByNamesStartWith(namesStartWith, 1, characterIds).then(resultCharacterIds => {
        if (resultCharacterIds == "" && namesStartWith != null && namesStartWith != "") {
            AddInnerHTML("No record found.", "comicsContainer")
        }
        else {
            var seriesIds = "";
            GetSerieIdsFilterByTitlesStartWith(titleStartWith, 1, seriesIds).then(resultSeriesIds => {
                if (resultSeriesIds == "" && titleStartWith != null && titleStartWith != "") {
                    AddInnerHTML("No record found.", "comicsContainer")
                }
                else {
                    var requestURL = GenerateApiURL(_dataTypes.comics)
                    if (resultCharacterIds != "") {
                        requestURL += "&characters=" + resultCharacterIds;
                    }
                    if (resultSeriesIds != "") {
                        requestURL += "&series=" + resultSeriesIds;
                    }
                    FetchListOfComics(requestURL);
                }
            });
        }
    });
    
}

function FetchListOfComics(requestUrl) {
    var XHR = new HttpRequest(requestUrl, null);
    XHR.executeRequest(function (result) {
        LoadComics(result.data.results);
        LoadPaging(currentPage, _pageSize, result.data.total);
    });
}

function LoadComics(liComics) {
    ClearElement("comicsContainer");
    var length = liComics.length;
    if (length < 1) {
        AddInnerHTML("No record found.", "comicsContainer")
    }
    else {
        for (var i = 0; i < length; i++) {
            GetBriefLayoutForComic(liComics[i]).then(sectionElement => {
                AddNewElement(sectionElement, "comicsContainer");
            });
        }
    }
}

function LoadPaging(currentPage, pagesize, totalRecord) {
    AddValue(currentPage, 'txtPage');
    var totalPage = Math.floor(totalRecord / pagesize) + (totalRecord % pagesize);
    AddInnerHTML(totalPage, 'lblTotalPage');
}


