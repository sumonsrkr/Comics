﻿

function PrepareBriefLayout(character) {
    var sectionElement = document.createElement("section");
    sectionElement.setAttribute("class", "inner-wrapper");

    var articleElement = document.createElement("article");

    var imgElement = document.createElement("img");
    imgElement.setAttribute('src', character.thumbnail.path + "/portrait_xlarge." + character.thumbnail.extension);


    var titleElement = document.createElement("h4");
    titleElement.innerHTML = character.name;

    articleElement.appendChild(imgElement);
    articleElement.appendChild(titleElement);

    var aElementDetails = document.createElement("a");
    aElementDetails.setAttribute('href', "CharacterDetails.html?id=" + character.id);
    aElementDetails.appendChild(articleElement);
    sectionElement.appendChild(aElementDetails);
    return sectionElement;
}

function GetDetailLayoutForCharacter(character) {
    var promise = new Promise(function (resolve, reject) {
        var sectionElement = document.createElement("section");
        sectionElement.setAttribute("class", "inner-wrapper");

        var articleElement = document.createElement("article");
        var asideElement = document.createElement("aside");

        var imgElement = document.createElement("img");
        imgElement.setAttribute('src', character.thumbnail.path + "/portrait_uncanny." + character.thumbnail.extension);


        var h3Element = document.createElement("h3");
        h3Element.innerHTML = character.name;

        var descElement = document.createElement("p");
        var description = "<b>Description</b></br>";
        if (character.description == null || character.description == "") {
            description += "</br>No description available.";
        }
        else {
            description += "</br>" + character.description;
        }
        descElement.innerHTML = description;

        var comicsElement = document.createElement("p");
        var comicsDetails = "<b>Comics</b></br>";
        if (character.comics.returned < 1) {
            comicsDetails += "</br>No comics available.";
        }
        else {
            for (var i = 0; i < character.comics.returned; i++) {
                comicsDetails += "</br>";
                var id = character.comics.items[i].resourceURI.slice(character.comics.items[i].resourceURI.lastIndexOf("/") + 1)
                comicsDetails += "&nbsp<a href='ComicDetails.html?id=" + id + "'>" + character.comics.items[i].name + "</a>";
            }
        }
        comicsElement.innerHTML = comicsDetails;

        var seriesElement = document.createElement("p");
        var seriesDetails = "<b>Series</b></br>";
        if (character.series.returned < 1) {
            seriesDetails += "</br>No series available.";
        }
        else {
            for (var i = 0; i < character.series.returned; i++) {
                seriesDetails += "</br>";
                seriesDetails += "&nbsp" + character.series.items[i].name;
            }
        }
        seriesElement.innerHTML = seriesDetails;

        var storiesElement = document.createElement("p");
        var storiesDetails = "<b>Stories</b></br>";
        if (character.stories.returned < 1) {
            storiesDetails += "</br>No stories available.";
        }
        else {
            for (var i = 0; i < character.stories.returned; i++) {
                storiesDetails += "</br>";
                storiesDetails += "&nbsp" + character.stories.items[i].name;
            }
        }
        storiesElement.innerHTML = storiesDetails;


        var eventsElement = document.createElement("p");
        var eventsDetails = "<b>Events</b></br>";
        if (character.events.returned < 1) {
            eventsDetails += "</br>No events available.";
        }
        else {
            for (var i = 0; i < character.events.returned; i++) {
                eventsDetails += "</br>";
                eventsDetails += "&nbsp" + character.events.items[i].name;
            }
        }
        eventsElement.innerHTML = eventsDetails;


        articleElement.appendChild(imgElement);

        asideElement.appendChild(h3Element);
        asideElement.appendChild(descElement);
        asideElement.appendChild(comicsElement);
        asideElement.appendChild(seriesElement);
        asideElement.appendChild(storiesElement);
        asideElement.appendChild(eventsElement);

        sectionElement.appendChild(articleElement);
        sectionElement.appendChild(asideElement);
        resolve(sectionElement);
    });
    return promise;
}