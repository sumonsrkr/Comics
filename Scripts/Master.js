﻿/*
Global Variables
*/
var _baseAPIUrl = "https://gateway.marvel.com//v1/public/";
//var _apiKey = "d2c8498dbb598067c6ef61be7fb36078";
var _apiKey = "ts=1&apikey=d2c8498dbb598067c6ef61be7fb36078&hash=8481cc68c79b86510a716303d6b3d09c";
var _pageSize = 20;

var _dataTypes = {
    comics: "comics",
    characters: "characters",
    series: "series"
};

/*
values of apiFor are comics, characters, series etc;
*/
function GenerateApiURL(apiFor) {
    return _baseAPIUrl + apiFor + "?" + _apiKey;
}

function HttpRequest(requestedURL) {
    this.XHR = new XMLHttpRequest();
    this.requestedURL = requestedURL;
    this.executeRequest = function (callBack) {
        // We define what will happen if the response is received from the server.
        this.XHR.addEventListener('load', function (event) {
            var response = JSON.parse(this.responseText);
            if (response.message != null) {
                alert(response.message);
            }
            else if (callBack != null) {
                callBack(response);
            }
        });    

        this.XHR.open("GET", this.requestedURL, true);

        // And finally, We submit our request.
        this.XHR.send();
    };
}


function GetCharacterIdsFilterByNamesStartWith(namesStartWith, pageNumber, resultIDs) {
    var promise = new Promise(function (resolve, reject) {
        if (namesStartWith == "")
        {
            resolve(resultIDs);
        }
        var offset = (pageNumber - 1) * _pageSize;
        GetCharactersFilterByNamesStartWith(namesStartWith, offset).then(result => {
            var length = result.data.count;
            for (var i = 0; i < length && i < 10; i++) {
                if (resultIDs != "") {
                    resultIDs += ","
                }
                resultIDs += result.data.results[i].id;
            }
            resolve(resultIDs);
        });
    });
    return promise;
}

function GetCharactersFilterByNamesStartWith(namesStartWith, offset) {    
    var promise = new Promise(function (resolve, reject) {
        var requestURLForCharacters = GenerateApiURL(_dataTypes.characters) + "&offset=" + offset;
        if (namesStartWith != null && namesStartWith != "") {
            requestURLForCharacters += "&nameStartsWith=" + namesStartWith;
        }
        var XHR = new HttpRequest(requestURLForCharacters, null);
        XHR.executeRequest(function (result) {
            resolve(result);
        });        
    });
    return promise;
}

function GetSerieIdsFilterByTitlesStartWith(titlesStartWith, pageNumber, resultIDs) {
    var promise = new Promise(function (resolve, reject) {
        if (titlesStartWith == "") {
            resolve(resultIDs);
        }
        var offset = (pageNumber - 1) * _pageSize;
        GetSeriesFilterByTitlesStartWith(titlesStartWith, offset).then(result => {
            var length = result.data.count;
            for (var i = 0; i < length && i < 10; i++) {
                if (resultIDs != "") {
                    resultIDs += ","
                }
                resultIDs += result.data.results[i].id;
            }
            resolve(resultIDs);
        });
    });
    return promise;
}

function GetSeriesFilterByTitlesStartWith(titlesStartWith, offset) {
    var promise = new Promise(function (resolve, reject) {
        var requestURLForSeries = GenerateApiURL(_dataTypes.series) + "&offset=" + offset;
        if (titlesStartWith != null && titlesStartWith != "") {
            requestURLForSeries += "&titleStartsWith=" + titlesStartWith;
        }
        var XHR = new HttpRequest(requestURLForSeries, null);
        XHR.executeRequest(function (result) {
            resolve(result);
        });
    });
    return promise;
}

function ClearElement(elementId) {
    var element = document.getElementById(elementId);
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

function AddNewElement(content, parentElementId) {
    var parentElement = document.getElementById(parentElementId);
    parentElement.appendChild(content);
}

function AddInnerHTML(content, parentElementId) {
    var parentElement = document.getElementById(parentElementId);
    parentElement.innerHTML = content;
}

function AddValue(content, parentElementId) {
    var parentElement = document.getElementById(parentElementId);
    parentElement.value = content;
}

function GetQueryStringByName(name) {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars[name];
}