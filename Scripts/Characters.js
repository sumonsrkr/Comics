﻿

FetchListOfCharacters(_baseAPIUrl + "characters" + "?" + _apiKey);

function FetchListOfCharacters(requestUrl) {
    var XHR = new HttpRequest(requestUrl, null);
    XHR.executeRequest(function (result) {
        LoadCharacters(result.data.results);
    });
}

function LoadCharacters(liCharacters) {
    var length = liCharacters.length;

    for (var i = 0; i < length; i++) {
        GetBriefLayoutForCharacter(liCharacters[i]).then(sectionElement => {
            AddNewElement(sectionElement, "charactersContainer");
        });
    }
}

function GetBriefLayoutForCharacter(character) {
    var promise = new Promise(function (resolve, reject) {
        resolve(PrepareBriefLayout(character));
    });
    return promise;
}

